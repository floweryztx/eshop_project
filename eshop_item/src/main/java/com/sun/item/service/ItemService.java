package com.sun.item.service;

import com.sun.commons.pojo.TbItemCatCustom;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;

public interface ItemService {
	
	TbItem loadTbItemById(long id);

	TbItemDesc loadTbItemDescByItemId(long item_id);
    
    TbItemParamItem loadTbItemParamItemByItemId(long item_id);
    
    TbItemCatCustom linkCat(Long catid);
    
    String TbItemParamHtml(String paramJsonString);
}
