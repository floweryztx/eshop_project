package com.sun.webmanage.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUITreeData;
import com.sun.commons.utils.ActionUtils;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbContentCategory;
import com.sun.webmanage.service.TbContentCategoryService;

@Controller
public class TbContentCategoryController {

	@Resource
	private TbContentCategoryService tbContentCategoryService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TbContentCategoryController.class);

	
	@ResponseBody
	@RequestMapping("/content/category/list")
	public List<EasyUITreeData> categoryList(@RequestParam(value="id",defaultValue="0")long pid){
		return tbContentCategoryService.listTbContentCategory(pid);
	}
	
	@ResponseBody
	@RequestMapping("/content/category/create")
	public Map<String,Object> addCategory(Long parentId,String name){
		TbContentCategory insertObj;
		try {
			insertObj = tbContentCategoryService.saveTbContentCategory(parentId, name);
			if(insertObj!=null){
				return ActionUtils.ajaxSuccess("新增成功", insertObj);
			}
		} catch (DaoException e) {
			e.printStackTrace();
			return ActionUtils.ajaxFail("新增失败", e.getMessage());	
		}
		
		return ActionUtils.ajaxFail("新增失败", "");	
	}
	
	@ResponseBody
	@RequestMapping("/content/category/delete/")
	public Map<String,Object> deleteCategory(Long parentId,Long id){
		boolean status = tbContentCategoryService.deleteTbContentCategory(parentId, id);
		if(status){
			return ActionUtils.ajaxSuccess("删除成功", "");
		}else{
			return ActionUtils.ajaxFail("删除失败", "");	
		}
	}
	
	@ResponseBody
	@RequestMapping("/content/category/update")
	public Map<String,Object> updateCategory(String name,Long id){
		boolean status;
		try {
			status = tbContentCategoryService.updateTbContentCategory(id, name);
			if(status){
				return ActionUtils.ajaxSuccess("更新成功", "");
			}
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.info("updateCategory error===="+e.getMessage());
			return ActionUtils.ajaxFail("更新失败", e.getMessage());
		}
			return ActionUtils.ajaxFail("更新失败", "");	
	}
	
}
