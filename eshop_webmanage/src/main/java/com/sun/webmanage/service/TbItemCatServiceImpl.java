package com.sun.webmanage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.EasyUITreeData;
import com.sun.dubbo.webmanage.service.TbItemCatDubboService;
import com.sun.webmanage.model.TbItemCat;

@Service("TbItemCatService")
public class TbItemCatServiceImpl implements TbItemCatService{

	@Reference
	private TbItemCatDubboService tbItemCatDubboService;

	@Override
	public List<EasyUITreeData> listEasyUITreeData(long pid) {
		List<TbItemCat> listdata = tbItemCatDubboService.listTbItemCatByPid(pid);
		List<EasyUITreeData> retList = new ArrayList<>();
		EasyUITreeData tree = null;
		for(TbItemCat cat:listdata){
			tree = new EasyUITreeData();
			tree.setId(cat.getId());
			tree.setText(cat.getName());
			tree.setState(cat.getIsParent()?"closed":"open");
			retList.add(tree);
		}
		return retList;
	}
}
