package com.sun.dubbo.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.TbItemCatCustom;
import com.sun.webmanage.model.TbItemCat;

public interface TbItemCatDubboService {

	/**
	 * 根据pid查找子类别列表
	 * @param pid 父类别id   根目录父类别id为0
	 * @return 子类别列表
	 */
	List<TbItemCat> listTbItemCatByPid(long pid);
	
	TbItemCat loadTbItemCatByPK(long id);
	
	/**
	 * 根据子类返回其上级类别链表
	 * @param leaf 叶子类别  链表链条中最低类别
	 * @return
	 */
	TbItemCatCustom linkCat(TbItemCatCustom leaf);
	
}
